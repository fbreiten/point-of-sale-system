import QtQuick 1.1

Item {
    id: bar
    width: 512
    height: 120

    property alias status: statuslbl.text
    state: "stage1"
    states: [
        State {
            name: "stage1"
            PropertyChanges {target: progress; source: "Loadingbar stage 1.png"}
            PropertyChanges {target: scanitemslbl; visible: true}
            PropertyChanges {target: scanninglbl; visible: false}
            PropertyChanges {target: paymentlbl; visible: false}
            PropertyChanges {target: statuslbl; visible: false}
            PropertyChanges {target: donelbl; visible: false}
            PropertyChanges {target: remaininglbl; visible: false}
        },
        State {
            name: "stage1loading"
            PropertyChanges {target: progress; source: "Loadingbar stag 1 - loading.png"}
            PropertyChanges {target: scanitemslbl; visible: true}
            PropertyChanges {target: scanninglbl; visible: true}
            PropertyChanges {target: paymentlbl; visible: false}
            PropertyChanges {target: statuslbl; visible: false}
            PropertyChanges {target: donelbl; visible: false}
            PropertyChanges {target: remaininglbl; visible: false}
        },
        State {
            name: "stage2"
            PropertyChanges {target: progress; source: "Loadingbar stage 2.png"}
            PropertyChanges {target: scanitemslbl; visible: true}
            PropertyChanges {target: scanninglbl; visible: false}
            PropertyChanges {target: paymentlbl; visible: true}
            PropertyChanges {target: statuslbl; visible: false}
            PropertyChanges {target: donelbl; visible: false}
            PropertyChanges {target: remaininglbl; visible: true}
        },
        State {
            name: "stage2loading"
            PropertyChanges {target: progress; source: "Loadingbar stag 2 - loading - waiting.png"}
            PropertyChanges {target: scanitemslbl; visible: true}
            PropertyChanges {target: scanninglbl; visible: false}
            PropertyChanges {target: paymentlbl; visible: true}
            PropertyChanges {target: statuslbl; visible: true}
            PropertyChanges {target: donelbl; visible: false}
            PropertyChanges {target: remaininglbl; visible: true}
        },
        State {
            name: "stage3failed"
            PropertyChanges {target: progress; source: "Loadingbar stage 3 - failed.png"}
            PropertyChanges {target: scanitemslbl; visible: true}
            PropertyChanges {target: scanninglbl; visible: false}
            PropertyChanges {target: paymentlbl; visible: true}
            PropertyChanges {target: statuslbl; visible: true}
            PropertyChanges {target: donelbl; visible: false}
            PropertyChanges {target: remaininglbl; visible: false}
        },
        State {
            name: "stage3bonus"
            PropertyChanges {target: progress; source: "Loadingbar stag 2 - loading - waiting.png"}
            PropertyChanges {target: scanitemslbl; visible: true}
            PropertyChanges {target: scanninglbl; visible: false}
            PropertyChanges {target: paymentlbl; visible: true}
            PropertyChanges {target: statuslbl; visible: true}
            PropertyChanges {target: donelbl; visible: false}
            PropertyChanges {target: remaininglbl; visible: false}
        },
        State {
            name: "stage3"
            PropertyChanges {target: progress; source: "Loadingbar stage 3.png"}
            PropertyChanges {target: scanitemslbl; visible: true}
            PropertyChanges {target: scanninglbl; visible: false}
            PropertyChanges {target: paymentlbl; visible: true}
            PropertyChanges {target: statuslbl; visible: false}
            PropertyChanges {target: donelbl; visible: true}
            PropertyChanges {target: remaininglbl; visible: false}
        }

    ]

    Image {
        id: progress
        y: 20
        anchors.centerIn: parent
        anchors.verticalCenterOffset: 20
        source: "Loadingbar stage 1.png"
    }

    Text {
        id: txt1lbl
        visible: false
        text: "1"
        font.pixelSize: 25
        color: "white"
        x: 49
        y: 63
    }

    Text {
        id: scanitemslbl
        text: "Scan \n Items"
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 15
        color: "white"
        x: -18
        y: 15
        width: 148
        height: 48
    }

    Text {
        id: scanninglbl
        visible: false
        text: "Scanning..."
        font.pixelSize: 15
        color: "white"
        x: 108
        y: 51
    }

    Text {
        id: paymentlbl
        visible: false
        text: "Payment"
        font.pixelSize: 15
        color: "white"
        x: 226
        y: 30
    }

    Text {
        id: statuslbl
        visible: false
        text: ""
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 15
        color: "white"
        x: 285
        y: 51
    }

    Text {
        id: txt2lbl
        visible: false
        x: 249
        y: 63
        color: "#ffffff"
        text: "2"
        font.pixelSize: 25
    }

    Text {
        id: txt3lbl
        visible: false
        x: 447
        y: 63
        color: "#ffffff"
        text: "3"
        font.pixelSize: 25
    }

    Text {
        id: donelbl
        visible: false
        x: 436
        y: 30
        color: "#ffffff"
        text: "Done"
        font.pixelSize: 16
    }
    Text {
        id: remaininglbl
        text: if(modelManager.remaining != "0.00" && modelManager.remaining != "-0.00")
                  "Remaining: " + modelManager.remaining + "€"
        else
                  ""
        visible: false
        font.pixelSize: 15
        color: "white"
        x: 285
        y: 85
    }
}
