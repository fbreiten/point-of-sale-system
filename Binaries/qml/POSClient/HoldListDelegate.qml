import QtQuick 1.1

Component {
    Rectangle {
        id: rectangle1
        clip: true //This is so the text is always inside the box
        height: 64
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#6d6d6d"
            }

            GradientStop {
                position: 1
                color: "#555555"
            }
        }
        //Animations
        ListView.onAdd: SequentialAnimation {
                         PropertyAction { target: rectangle1; property: "height"; value: 0 }
                         NumberAnimation { target: rectangle1; property: "height"; to: 64; duration: 250; easing.type: Easing.InOutQuad }
                     }

        ListView.onRemove: SequentialAnimation {
                         PropertyAction { target: rectangle1; property: "ListView.delayRemove"; value: true }
                         NumberAnimation { target: rectangle1; property: "height"; to: 0; duration: 250; easing.type: Easing.InOutQuad }
                         PropertyAction { target: rectangle1; property: "ListView.delayRemove"; value: false}
        }

        MouseArea {
            anchors.fill: parent
            onClicked: hlist.currentIndex = index //Saves the the index of clicked item
        }

        border.color: "#000000"
        anchors.right: parent.right
        anchors.left: parent.left

        states: State {
                         name: "Current"
                         when: rectangle1.ListView.isCurrentItem
                         PropertyChanges { target: rectangle1; border.color: "lightsteelblue" }
                     }
        property int minutes : 0
        Timer {
                   interval: 60000
                   running: true
                   repeat: true
                   onTriggered: {
                       minutes++
                   }
               }

               Text {
                   id: nameLabel
                   color: "#f1f1f1"
                   text: name
                   anchors.centerIn: parent
                   font.pixelSize: 16
               }
               Text {
                   id:timeAgo
                   anchors.top: nameLabel.bottom
                   anchors.horizontalCenter: parent.horizontalCenter
                   color: "#f1f1f1"
                   text: minutes +" min ago"
                   font.italic: true
                   font.pixelSize: 12
               }
    }
}
