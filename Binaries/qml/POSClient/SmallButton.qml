import QtQuick 1.1

Rectangle {

    property color buttonColor: "#9e3737"
    property alias text: buttonLabel.text

    id: clearButton
    width: 99
    height: 30
    radius: 0

    gradient: Gradient {
        GradientStop {
            position: 0
            color: "#7a0d0d"
        }

        GradientStop {
            position: 1
            color: buttonMouseArea.pressed ? Qt.darker(buttonColor, 1.5) : buttonColor
        }
    }
    smooth: true

    Text {
        id: buttonLabel
        x: 15
        y: 6
        color: "#e6d7d7"
        text: qsTr("Clear list")
        font.family: "Arial"
        font.bold: true
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 17
    }

    MouseArea {
        id: buttonMouseArea
        x: 0
        y: 0
        hoverEnabled: true
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        anchors.fill: parent

        onClicked: modelManager.clearList()
    }
}
