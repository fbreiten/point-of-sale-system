// This is the FunctionGrid, it contain rows of buttons, the buttons are all centralized.
import QtQuick 1.1

Rectangle {
    width: 512
    height: 350

    Item {
        id:row1
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        height: 95

        Row {
            anchors.centerIn: parent
            spacing: 8
            Button {
                text: "-1"
                buttonColor: "#7995CF"
                onButtonClick: modelManager.decrementQuantity(list.currentIndex, 1)
            }
            Button {
                text: "+1"
                buttonColor: "#97B88D"
                onButtonClick: modelManager.incrementQuantity(list.currentIndex, 1)
            }
            Button {
                text: "+5"
                buttonColor: "#B58D54"
                onButtonClick: modelManager.incrementQuantity(list.currentIndex, 5)
            }
            Button {
                text: "+10"
                buttonColor: "#965D60"
                onButtonClick: modelManager.incrementQuantity(list.currentIndex, 10)
            }
        }
    }
    Item {
        id: row2
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: row1.bottom
        height: 95
        Row {
            anchors.centerIn: parent
            spacing: 8
            Button {
                text: "10%"
                buttonColor: "#7995CF"
                onButtonClick: modelManager.setDiscount(list.currentIndex, 10)
            }
            Button {
                text: "25%"
                buttonColor: "#97B88D"
                onButtonClick: modelManager.setDiscount(list.currentIndex, 25)
            }
            Button {
                text: "50%"
                buttonColor: "#B58D54"
                onButtonClick: modelManager.setDiscount(list.currentIndex, 50)
            }
            Button {
                text: "75%"
                buttonColor: "#965D60"
                onButtonClick: modelManager.setDiscount(list.currentIndex, 75)
            }
        }
    }

    Item {
        id: row4
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: row2.bottom
        height: 95
        Row {
            anchors.centerIn: parent
            spacing: 8
            Button {
                text: "Card"
                buttonColor: "#FFA700"
                onButtonClick: {
                    if(informationBar.state != "stage3" && listModel.rowCount() > 0) {
                        cardReader.begin(payAmount.text); payAmount.text = ""
                        informationBar.state = "stage2"
                    }
                }
            }
            Button {
                text: "Cash"
                buttonColor: "#70D0FF"
                onButtonClick: {
                    if(informationBar.state != "stage3" && listModel.rowCount() > 0 && informationBar.state != "stage2loading")
                    {
                        cashBox.open(payAmount.text); payAmount.text = ""
                        informationBar.state = "stage2"
                    }
                }
            }

        }
    }
    Item {
        id: row5
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: row4.bottom
        height: 50
        Row {
            anchors.centerIn: parent
            spacing: 8
            InputField {
                id: payAmount
                width: 248
                height: 30
            }
        }
    }

    Button {
        id: cancelButton
        x: 386
        y: 195
        text: "Cancel"
        buttonColor: "red"
        visible: false
        onButtonClick: {
            cardReader.reset()
            informationBar.state = "stage1loading"
        }
    }

    states: State {
         name: "Paying"
         when: informationBar.state != "stage1loading" && informationBar.state != "stage1" && informationBar.state != "stage3"
         PropertyChanges { target: row2; visible:false }
         PropertyChanges { target: row1; visible:false }
         PropertyChanges { target: holdButton; visible:false }
         PropertyChanges { target: restoreButton; visible:false }
         PropertyChanges { target: receiptButton; visible:false }
         PropertyChanges { target: cancelButton; visible:true }
         PropertyChanges { target: clearButton; visible:false }
         PropertyChanges { target: inputBox; disable: true; toggleFocus: false }
     }
}
