import QtQuick 1.1

ShadowedRectangle {

    property alias text: buttonLabel.text
    property color buttonColor: "#535353"

    id: button
    width: 120
    height: 85
    smooth: true
    //use onButtonClick: when you want an action
    signal buttonClick()

    color: buttonMouseArea.pressed ? Qt.darker(buttonColor, 1.5) : buttonColor

    Text {
        id: buttonLabel
        anchors.centerIn:  parent
        font.pointSize: 18
        text:  "button label"
        color: "white"
    }

    MouseArea {
        id: buttonMouseArea
        anchors.fill: parent
        onClicked: buttonClick()
    }
}
