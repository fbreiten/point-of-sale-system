from django.contrib import admin
from POSDatabase.models import Customer, Product, SaleData, SaleEntity

class CustomerAdmin(admin.ModelAdmin):
    list_display = ('customer_id', 'first_name', 'birth_date', 'phone_nr', 'address', 'payment_card_nr', 'bonus_card_nr')

admin.site.register(Customer, CustomerAdmin)

class ProductAdmin(admin.ModelAdmin):
    list_display = ('product_id', 'name', 'price', 'discount')

admin.site.register(Product, ProductAdmin)

class SaleDataAdmin(admin.ModelAdmin):
    list_display = ('sale_id', 'product_id', 'product_quantity')

admin.site.register(SaleData, SaleDataAdmin)

class SaleEntityAdmin(admin.ModelAdmin):
    list_display = ('sale_id', 'total_sum', 'customer_id', 'sale_date', 'bonus_points')

admin.site.register(SaleEntity, SaleEntityAdmin)