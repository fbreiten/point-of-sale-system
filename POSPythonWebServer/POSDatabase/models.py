from django.db import models

class Customer(models.Model):
    customer_id = models.BigIntegerField(primary_key=True)
    first_name = models.CharField(max_length=50L, blank=True)
    last_name = models.CharField(max_length=50L, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    phone_nr = models.BigIntegerField()
    address = models.CharField(max_length=50L, blank=True)
    payment_card_nr = models.BigIntegerField()
    bonus_card_nr = models.BigIntegerField()
    class Meta:
        db_table = 'customer'

class Product(models.Model):
    product_id = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=50L, blank=True)
    price = models.DecimalField(null=True, max_digits=12, decimal_places=2, blank=True)
    discount = models.DecimalField(null=0, max_digits=12, decimal_places=2, blank=True)
    class Meta:
        db_table = 'product'

class SaleData(models.Model):
    sale_id = models.BigIntegerField(primary_key=True)
    product_id = models.BigIntegerField(primary_key=True)
    product_quantity = models.IntegerField()
    class Meta:
        db_table = 'sale_data'

class SaleEntity(models.Model):
    sale_id = models.BigIntegerField(primary_key=True)
    total_sum = models.DecimalField(null=True, max_digits=12, decimal_places=2, blank=True)
    customer_id = models.BigIntegerField()
    sale_date = models.DateField(null=True, blank=True)
    bonus_points = models.DecimalField(null=True, max_digits=7, decimal_places=2, blank=True)
    class Meta:
        db_table = 'sale_entity'