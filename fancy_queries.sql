
## Select top three products between two dates
SELECT p.name, SUM(product_quantity) as quantity FROM sale_entity se
	INNER JOIN sale_data sd USING(sale_id)
	INNER JOIN product p USING(product_id)
	WHERE se.sale_date >= '2013-04-01' AND se.sale_date <= '2013-04-10'
	GROUP BY p.product_id
	ORDER BY quantity DESC
	LIMIT 3;

## Select bottom three products between two dates
SELECT p.product_id, p.name, SUM(product_quantity) as quantity FROM sale_entity se
	INNER JOIN sale_data sd USING(sale_id)
	INNER JOIN product p USING(product_id)
	WHERE se.sale_date >= '2013-04-01' AND se.sale_date <= '2013-04-10'
	GROUP BY p.product_id
	ORDER BY quantity ASC
	LIMIT 3;
