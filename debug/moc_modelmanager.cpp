/****************************************************************************
** Meta object code from reading C++ file 'modelmanager.h'
**
** Created: Sun 14. Apr 21:10:51 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../POSClient/modelmanager.h"
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'modelmanager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ModelManager[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       4,   99, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x05,
      31,   13,   13,   13, 0x05,

 // methods: signature, parameters, type, tag, flags
      53,   50,   13,   13, 0x02,
      83,   77,   13,   13, 0x02,
     102,   13,   13,   13, 0x02,
     127,  114,   13,   13, 0x02,
     154,   77,   13,   13, 0x02,
     191,  177,   13,   13, 0x02,
     214,   13,   13,   13, 0x02,
     227,   13,   13,   13, 0x02,
     252,   13,  244,   13, 0x02,
     273,   13,  267,   13, 0x02,
     287,   13,   13,   13, 0x02,
     298,   77,   13,   13, 0x02,
     315,   13,   13,   13, 0x02,
     330,   13,  267,   13, 0x02,
     346,   13,   13,   13, 0x02,

 // properties: name, type, flags
     368,  244, 0x0a495103,
     373,  267, ((uint)QMetaType::QReal << 24) | 0x00495103,
     379,  244, 0x0a495001,
     389,  267, ((uint)QMetaType::QReal << 24) | 0x00495001,

 // properties: notify_signal_id
       0,
       0,
       1,
       0,

       0        // eod
};

static const char qt_meta_stringdata_ModelManager[] = {
    "ModelManager\0\0currentChanged()\0"
    "remainingUpdated()\0id\0addProductById(QString)\0"
    "index\0removeProduct(int)\0clearList()\0"
    "index,amount\0incrementQuantity(int,int)\0"
    "decrementQuantity(int)\0index,percent\0"
    "setDiscount(int,qreal)\0changeList()\0"
    "resetRemaining()\0QString\0getRemaining()\0"
    "qreal\0getDiscount()\0holdSale()\0"
    "restoreSale(int)\0printReciept()\0"
    "getAmountPaid()\0setAmountPaidToZero()\0"
    "name\0price\0remaining\0discount\0"
};

void ModelManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ModelManager *_t = static_cast<ModelManager *>(_o);
        switch (_id) {
        case 0: _t->currentChanged(); break;
        case 1: _t->remainingUpdated(); break;
        case 2: _t->addProductById((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->removeProduct((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->clearList(); break;
        case 5: _t->incrementQuantity((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 6: _t->decrementQuantity((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->setDiscount((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< qreal(*)>(_a[2]))); break;
        case 8: _t->changeList(); break;
        case 9: _t->resetRemaining(); break;
        case 10: { QString _r = _t->getRemaining();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 11: { qreal _r = _t->getDiscount();
            if (_a[0]) *reinterpret_cast< qreal*>(_a[0]) = _r; }  break;
        case 12: _t->holdSale(); break;
        case 13: _t->restoreSale((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->printReciept(); break;
        case 15: { qreal _r = _t->getAmountPaid();
            if (_a[0]) *reinterpret_cast< qreal*>(_a[0]) = _r; }  break;
        case 16: _t->setAmountPaidToZero(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ModelManager::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ModelManager::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ModelManager,
      qt_meta_data_ModelManager, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ModelManager::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ModelManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ModelManager::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ModelManager))
        return static_cast<void*>(const_cast< ModelManager*>(this));
    return QObject::qt_metacast(_clname);
}

int ModelManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = getName(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = getPrice(); break;
        case 2: *reinterpret_cast< QString*>(_v) = getRemaining(); break;
        case 3: *reinterpret_cast< qreal*>(_v) = getDiscount(); break;
        }
        _id -= 4;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setName(*reinterpret_cast< QString*>(_v)); break;
        case 1: setPrice(*reinterpret_cast< qreal*>(_v)); break;
        }
        _id -= 4;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void ModelManager::currentChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void ModelManager::remainingUpdated()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
QT_END_MOC_NAMESPACE
