/****************************************************************************
** Meta object code from reading C++ file 'cashbox.h'
**
** Created: Sun 14. Apr 21:10:50 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../POSClient/cashbox.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'cashbox.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CashBox[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       1,   39, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
       9,    8,    8,    8, 0x05,

 // slots: signature, parameters, type, tag, flags
      31,   24,    8,    8, 0x0a,

 // methods: signature, parameters, type, tag, flags
      60,   53,    8,    8, 0x02,
      79,    8,   74,    8, 0x02,
      91,    8,    8,    8, 0x02,

 // properties: name, type, flags
      24,   74, 0x01495001,

 // properties: notify_signal_id
       0,

       0        // eod
};

static const char qt_meta_stringdata_CashBox[] = {
    "CashBox\0\0updateStatus()\0status\0"
    "setStatus(QByteArray)\0amount\0open(QString)\0"
    "bool\0getStatus()\0poll()\0"
};

void CashBox::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CashBox *_t = static_cast<CashBox *>(_o);
        switch (_id) {
        case 0: _t->updateStatus(); break;
        case 1: _t->setStatus((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 2: _t->open((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: { bool _r = _t->getStatus();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 4: _t->poll(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CashBox::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CashBox::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_CashBox,
      qt_meta_data_CashBox, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CashBox::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CashBox::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CashBox::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CashBox))
        return static_cast<void*>(const_cast< CashBox*>(this));
    return QObject::qt_metacast(_clname);
}

int CashBox::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = getStatus(); break;
        }
        _id -= 1;
    } else if (_c == QMetaObject::WriteProperty) {
        _id -= 1;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void CashBox::updateStatus()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
