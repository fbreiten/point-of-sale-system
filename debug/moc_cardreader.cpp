/****************************************************************************
** Meta object code from reading C++ file 'cardreader.h'
**
** Created: Sun 14. Apr 21:10:49 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../POSClient/cardreader.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'cardreader.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CardReader[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       2,   59, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x05,
      26,   11,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
      47,   41,   11,   11, 0x0a,
      69,   41,   11,   11, 0x0a,

 // methods: signature, parameters, type, tag, flags
      91,   11,   11,   11, 0x02,
     106,   11,   98,   11, 0x02,
     117,   11,   98,   11, 0x02,
     136,  129,   11,   11, 0x02,
     151,   11,   11,   11, 0x02,

 // properties: name, type, flags
     159,   98, 0x0a495001,
     165,   98, 0x0a495001,

 // properties: notify_signal_id
       0,
       1,

       0        // eod
};

static const char qt_meta_stringdata_CardReader[] = {
    "CardReader\0\0stateUpdate()\0statusUpdate()\0"
    "array\0setStatus(QByteArray)\0"
    "setResult(QByteArray)\0poll()\0QString\0"
    "getState()\0getStatus()\0amount\0"
    "begin(QString)\0reset()\0state\0status\0"
};

void CardReader::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CardReader *_t = static_cast<CardReader *>(_o);
        switch (_id) {
        case 0: _t->stateUpdate(); break;
        case 1: _t->statusUpdate(); break;
        case 2: _t->setStatus((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 3: _t->setResult((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 4: _t->poll(); break;
        case 5: { QString _r = _t->getState();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 6: { QString _r = _t->getStatus();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 7: _t->begin((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 8: _t->reset(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CardReader::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CardReader::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_CardReader,
      qt_meta_data_CardReader, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CardReader::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CardReader::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CardReader::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CardReader))
        return static_cast<void*>(const_cast< CardReader*>(this));
    return QObject::qt_metacast(_clname);
}

int CardReader::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = getState(); break;
        case 1: *reinterpret_cast< QString*>(_v) = getStatus(); break;
        }
        _id -= 2;
    } else if (_c == QMetaObject::WriteProperty) {
        _id -= 2;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void CardReader::stateUpdate()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void CardReader::statusUpdate()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
QT_END_MOC_NAMESPACE
