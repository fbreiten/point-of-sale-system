
#define private public
#include <QtTest/QtTest>
#include "../POSClient/modelmanager.h"
#include "../POSClient/cardreader.h"
#include "../POSClient/cashbox.h"

class testList: public QObject
{
    Q_OBJECT
private slots:
    void addToList();
    void changeAmount();
    void removeFromList();
};

void testList::addToList()
{
    DBHandler db;
    db.createConnection();

    ModelManager manager(0, &db);
    CardReader cardReader(&manager);
    CashBox cashbox(&manager);

    //Kom ihåg att checka namn och ID etc. innan du kör testet!
    manager.addProductById("1");
    QCOMPARE(manager.getName(), QString("Kaffe"));
    QCOMPARE(manager.getPrice(), QString("4.80"));
}

void testList::changeAmount()
{
    DBHandler db;
    db.createConnection();

    ModelManager manager(0, &db);
    CardReader cardReader(&manager);
    CashBox cashbox(&manager);

    //Kom ihåg att checka namn och ID etc. innan du kör testet!
    manager.addProductById("1");
    manager.addProductById("1");
    QCOMPARE(manager.getListSize(), 1);
    ListItem* item = manager.m_pSaleModel->getItemAtIndex(0);
    ListProduct* casted = static_cast<ListProduct*>(item);
    QCOMPARE(casted->quantity(), 2);
    manager.addProductById("2");
    QCOMPARE(manager.getListSize(), 2);
}

void testList::removeFromList()
{
    DBHandler db;
    db.createConnection();

    ModelManager manager(0, &db);
    CardReader cardReader(&manager);
    CashBox cashbox(&manager);

    manager.addProductById("1");
    QCOMPARE(manager.getListSize(), 1);
    manager.removeProduct(0);
    QCOMPARE(manager.getListSize(), 0);
}

//QTEST_MAIN(testList)

#include "testList.moc"
