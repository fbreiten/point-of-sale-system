//#define private public
#include <QtTest/QtTest>
#include <QDomDocument>
#include <QXmlInputSource>
#include <QMetaType>
#include "../POSClient/Rest.h"

class testRest: public QObject
{
    Q_OBJECT
private slots:
    void restGet();
};

void testRest::restGet()
{
    qRegisterMetaType<QByteArray>("QByteArray");

    rest myRest;
    QSignalSpy spy(&myRest, SIGNAL(replied(QByteArray)));

    myRest.get(QUrl("http://www.w3schools.com/xml/note.xml"));
    QVERIFY(spy.isValid());
    QCOMPARE(spy.count(), 1);

    QDomDocument doc;
    QXmlInputSource is;
    is.setData(qvariant_cast<QByteArray>(spy.at(0).at(0)));
    doc.setContent(is.data());
    QDomNodeList list = doc.elementsByTagName("to");
    QCOMPARE(list.at(0).toElement().text(), QString("Tove"));

    //QCOMPARE(manager.getName(), QString("Kaffe"));
    //QCOMPARE(manager.getPrice(), qreal(4.80));
}

//QTEST_MAIN(testRest)

#include "testRest.moc"
