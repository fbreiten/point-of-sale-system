#-------------------------------------------------
#
# Project created by QtCreator 2013-04-15T10:58:30
#
#-------------------------------------------------

QT       += core
QT       += sql network xml
CONFIG  += qtestlib

TARGET = Testing
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += testList.cpp \
    ../POSClient/Rest.cpp \
    ../POSClient/ReceiptCreator.cpp \
    ../POSClient/modelmanager.cpp \
    ../POSClient/ListProduct.cpp \
    ../POSClient/ListModel.cpp \
    ../POSClient/ListHold.cpp \
    ../POSClient/DBHandler.cpp \
    ../POSClient/Customer.cpp \
    ../POSClient/CashBox.cpp \
    ../POSClient/CardReader.cpp \
    testRest.cpp \
    testDB.cpp

HEADERS += \
    ../POSClient/Rest.h \
    ../POSClient/receiptcreator.h \
    ../POSClient/modelmanager.h \
    ../POSClient/listproduct.h \
    ../POSClient/listmodel.h \
    ../POSClient/listhold.h \
    ../POSClient/dbhandler.h \
    ../POSClient/customer.h \
    ../POSClient/cashbox.h \
    ../POSClient/cardreader.h
