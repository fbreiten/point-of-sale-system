#define private public
#include <QtTest/QtTest>
#include "../POSClient/DBHandler.h"
#include "../POSClient/customer.h"
#include "../POSClient/modelmanager.h"
#include "../POSClient/cardreader.h"
#include "../POSClient/cashbox.h"


class testDB: public QObject
{
    Q_OBJECT
private slots:
    void dbInsertCustomerData();
    void dbRetrieveCustomerData();
    void dbInsertSale();
};

void testDB::dbInsertCustomerData()
{
    DBHandler* db = new DBHandler();
    Customer* customer = new Customer();
    db->createConnection();

    customer->m_customerId = "9";
    customer->m_firstName = "Testing";
    customer->m_lastName = "Test";
    customer->m_birthDate = "1.01.2001";
    customer->m_bonusCardNumber = "11";
    customer->m_goodThroughMonth = "11";
    customer->m_goodThroughYear = "2011";
    customer->m_blocked = "false";
    customer->m_expired = "false";
    customer->m_holderName = "Testing Test";
    customer->m_sex = "Male";

    customer->m_address = "asdf";
    customer->m_postalCode = "123";
    customer->m_postOffice = "turku";
    customer->m_country = "qwe";

    QVERIFY(db->writeCustomerData(customer));
    delete db;
}

void testDB::dbRetrieveCustomerData()
{
    DBHandler* db = new DBHandler();
    db->createConnection();

    db->retrieveCustomerIDWithBonusID("11");

    QCOMPARE(db->m_customerID, static_cast<qint64>(9));
    delete db;
}

void testDB::dbInsertSale()
{
    DBHandler db;
    db.createConnection();

    ModelManager manager(0, &db);
    CardReader cardReader(&manager);
    CashBox cashbox(&manager);

    //Kom ihåg att checka namn och ID etc. innan du kör testet!
    manager.addProductById("1"); //Kaffe 3.20
    manager.addProductById("1");

    manager.m_bonusCard = "11";
    manager.m_goodThroughYear = "2011";
    manager.m_goodThroughMonth = "11";

    QVERIFY(manager.addSaleToDatabase());
}

QTEST_MAIN(testDB)

#include "testDB.moc"
