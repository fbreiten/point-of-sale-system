
def join_url(url, *args):
    segments = url.strip('/').split('/')
    for segment in args:
        segments.append(str(segment))
    return '/'.join(segments)

