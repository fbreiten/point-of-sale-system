import urllib2 as url
from xml.dom import minidom
from helpers import join_url

class ProductNotFound(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class Product(object):

    fields = ['name', 'vat', 'barCode']

    def __repr__(self):
        return "<Product: name=%s barCode=%s>" % (self.name, self.barCode)
   
    @staticmethod
    def create_from_xml(xml):
        p = Product()
        for field in Product.fields:
            value = xml.getElementsByTagName(field)[0].firstChild.nodeValue
            setattr(p, field, value)
        return p

class ProductCatalog(object):

    def __init__(self, url):
        self.url = url

    def _rest_request(self, method, *args):
        u = self.url
        u = join_url(u, method)
        for arg in args:
            u = join_url(u, arg)
        response = url.urlopen(u)
        return minidom.parse(response)
    
    def _create_product_dict(self, xml):
        products = {}
        for node in xml.firstChild.childNodes:
            p = Product.create_from_xml(node)
            products.update(((p.barCode,p),))
        return products

    def findByBarCode(self, barcode):
        xml = self._rest_request('findByBarCode', barcode)
        return Product.create_from_xml(xml.firstChild)

    def findByName(self, name):
        xml = self._rest_request('findByName', name)
        return self._create_product_dict(xml)

    def findByKeyword(self, keyword):
        xml = self._rest_request('findByKeyword', keyword)
        return self._create_product_dict(xml)

    def find(self, data):
        result = {}
        
        try:
            p = self.findByBarCode(data)
            result.update(((p.barCode,p),))
        except: pass

        result.update(self.findByName(data))
        result.update(self.findByKeyword(data))
        
        if len(result) == 0:
            raise ProductNotFound("Product '%s' was not found." % data)

        return result

if __name__ == "__main__":
    p = ProductCatalog('http://localhost:9003/rest/')
    a = p.findByBarCode('100')

