from flask import Flask, render_template, g, request, flash, abort, redirect, url_for
from productcatalog import ProductCatalog, Product, ProductNotFound
import MySQLdb
import urllib2

app = Flask(__name__)
app.config.from_object('app.config')
app.config.from_envvar('SALESMAN_SETTINGS', silent=True)


def connect_db():
    return MySQLdb.connect(
            host=app.config['DBHOST'],
            user=app.config['DBUSER'],
            passwd=app.config['DBPASS'],
            db=app.config['DBNAME'],
            charset='utf8')

@app.before_request
def before_request():
    g.db = connect_db()
    g.catalog = ProductCatalog(app.config['PRODUCT_CATALOG_URI'])

@app.teardown_request
def teardown_request(exception):
    if hasattr(g, 'db'):
        g.db.close()

@app.route('/')
@app.route('/products')
def mainpage():
    c = g.db.cursor()
    c.execute('SELECT product_id, name, price, discount FROM product ORDER BY product_id ASC')
    products = c.fetchall()
    return render_template('products.html', products=products)

@app.route('/catalog/search')
def catalog_search_form():
    if request.args.has_key('product'):
        param = request.args['product'].encode('utf8')
        try:
            products = g.catalog.find(param)
            return render_template('catalog_results.html', result=products.values())
        except ProductNotFound:
            return render_template('error.html', message="Could not find that product =(")
        except urllib2.HTTPError:
            return render_template('error.html', message="Could not find that product =(")
        except urllib2.URLError:
            return render_template('error.html', message="Could not connect to product catalog")
    else:
        return render_template('search_catalog.html')

@app.route('/product/add/<int:barcode>', methods = ['GET','POST'])
def add_product(barcode):
    try:
        if request.method == 'POST':
            # process request
            price = request.form['price'] or 50000
            discount = request.form['discount'] or 0
            p = g.catalog.findByBarCode(barcode)
            c = g.db.cursor()
            c.execute('insert into product (product_id, name, price, discount) values (%s, %s, %s, %s)',
                    (barcode, p.name, price, discount))
            flash(u'Added product \'%s\' to the database with price \'%s\'' % (p.name, price))
            return redirect('/products')
    except MySQLdb.IntegrityError:
        flash('Product already exists! Use edit instead.');
        return redirect('/products')
    product = g.catalog.findByBarCode(barcode)
    return render_template('add_product.html', product=product)
    
@app.route('/product/delete/<int:barcode>')
def delete_product(barcode):
    c = g.db.cursor()
    c.execute('delete from product where product_id=%s', (barcode,))
    flash('Product \'%s\' was deleted.' % barcode)
    return redirect('/products')
    
@app.route('/product/edit/<int:barcode>', methods = ['GET', 'POST'])
def edit_product(barcode):
    if request.method == 'POST':
        try:
            price = request.form['price']
            discount = request.form['discount']
            c = g.db.cursor()
            c.execute('update product set price=%s, discount=%s where product_id=%s', (price, discount, barcode))
            flash('Product updated!')
            return redirect('/products')
        except MySQLdb.OperationalError as e:
            flash('The database did not accept your data')
            flash(e.args[1])
            return redirect('/products')
    c = g.db.cursor(MySQLdb.cursors.DictCursor)
    c.execute('select product_id, name, price, discount from product where product_id=%s', barcode)
    p = c.fetchone()
    if not p: return render_template('error.html', message="No such product =(")
    return render_template('edit_product.html', product=p)
