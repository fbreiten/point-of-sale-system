
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `customer_id` bigint unsigned NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `sex` varchar(20) NOT NULL,
  `birth_date` date NOT NULL,
  `address` varchar(50) NOT NULL,
  `postal_code` varchar(50) NOT NULL,
  `post_office` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `product_id` bigint(20) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discount` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `sale_data`;
CREATE TABLE `sale_data` (
  `sale_id` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `product_quantity` smallint(6) NOT NULL,
  PRIMARY KEY (`sale_id`,`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `sale_entity`;
CREATE TABLE `sale_entity` (
  `sale_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `total_sum` decimal(10,2) NOT NULL,
  `customer_id` bigint(20) unsigned NOT NULL,
  `sale_date` datetime NOT NULL,
  `bonus_points` decimal(7,2) NOT NULL,
  PRIMARY KEY (`sale_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

drop table if exists bonus_card;
create table bonus_card (
customer_id BIGINT NOT NULL,
blocked varchar(5),
expired varchar(5),
good_through_month INT,
good_through_year INT,
bonus_card_nr BIGINT NOT NULL,
PRIMARY KEY (bonus_card_nr)
) ENGINE=MyISAM;