#include "cardreader.h"
#include "rest.h"
#include <QDomDocument>
#include <QXmlInputSource>

CardReader::CardReader(ModelManager *manager, QObject *parent) :
    m_pmanager(manager),QObject(parent)
{
    m_pCardReader = new rest();
    m_pCardReaderXML = new rest();

    connect(m_pCardReader, SIGNAL(replied(QByteArray)),
                    this, SLOT(setStatus(QByteArray)));

    connect(m_pCardReaderXML, SIGNAL(replied(QByteArray)),
            this, SLOT(setResult(QByteArray)));

    m_pCardReader->post(QUrl("http://localhost:9002/cardreader/abort"),"");
    m_pCardReader->post(QUrl("http://localhost:9002/cardreader/reset"),"");
}

CardReader::~CardReader()
{
    delete m_pCardReader;
    delete m_pCardReaderXML;
}


//begin prepares the cardreader for a new transaction
void CardReader::begin(QString amount)
{
    ListModel *model = m_pmanager->getSaleModel();
    if (model->rowCount() != 0) {

        if(amount.isEmpty()){
            qreal remaining = model->getSum().toFloat() - m_pmanager->getAmountPaid();
            if (remaining == 0) {
                amount = model->getSum();
            } else {
                amount = QString::number(remaining, 'f', 2);
            }
        }
        m_ack = false;
        m_state = "";
        m_pCardReader->post(QUrl("http://localhost:9002/cardreader/abort"),"");
        m_pCardReader->post(QUrl("http://localhost:9002/cardreader/reset"),"");
        m_pCardReader->post(QUrl("http://localhost:9002/cardreader/waitForPayment"),"amount=" + amount);

        m_cardAmount = amount.toFloat();
        emit stateUpdate();

    }
}

//This function is called from RestPolling.qml at an interval to check it the status has updated
void CardReader::poll()
{
    m_pCardReader->get(QUrl("http://localhost:9002/cardreader/status"));
}

//setStatus slot is called when the status has been retrieved through the polling function
//it then checks if the cardreader is done and has not been called before.
void CardReader::setStatus(QByteArray array)
{
    if(QString(array).compare(m_status))
    {
        m_status = QString(array);
        emit statusUpdate();
        qDebug() << "status: " << m_status;

        if(m_status.contains("DONE") && !m_ack)
        {
            m_ack = true;
            m_pCardReaderXML->get(QUrl("http://localhost:9002/cardreader/result"));
        }
    } else {
    }
}

//This slot gets the array when rest has retrieved the results
//The function contains a XML parser which will add the result to SaleResult
void CardReader::setResult(QByteArray array)
{
    QDomDocument doc;
    QXmlInputSource is;
    is.setData(array);
    doc.setContent(is.data());

    QDomNodeList paymentStateList = doc.elementsByTagName("paymentState");
    QDomNodeList bonusStateList = doc.elementsByTagName("bonusState");

    if(bonusStateList.size() > 0)
    {
        m_state = "bonus" + bonusStateList.at(0).toElement().text();
        emit stateUpdate();
        if(bonusStateList.at(0).toElement().text().contains("ACCEPTED"))
        {
            bonusStateList = doc.elementsByTagName("bonusCardNumber");

            if(bonusStateList.size() > 0) {
                m_pmanager->setBonus(bonusStateList.at(0).toElement().text());
            }
            bonusStateList = doc.elementsByTagName("goodThruMonth");
            if(bonusStateList.size() > 0) {
                m_pmanager->setGoodThroughMonth(bonusStateList.at(0).toElement().text());
            }
            bonusStateList = doc.elementsByTagName("goodThruYear");
            if(bonusStateList.size() > 0) {
                m_pmanager->setGoodThroughYear(bonusStateList.at(0).toElement().text());
            }
        }
    }

    if(paymentStateList.size() > 0)
    {
        m_state = paymentStateList.at(0).toElement().text();
        emit stateUpdate();
        if(paymentStateList.at(0).toElement().text().contains("ACCEPTED"))
        {

            paymentStateList = doc.elementsByTagName("paymentCardNumber");

            if(paymentStateList.size() > 0) {
                m_pmanager->setPayment(paymentStateList.at(0).toElement().text());
                m_pmanager->prepareFinalize(m_cardAmount);
            }
        }
    }

}

void CardReader::reset()
{
    m_pCardReader->post(QUrl("http://localhost:9002/cardreader/abort"),"");
    m_pCardReader->post(QUrl("http://localhost:9002/cardreader/reset"),"");
    m_state = "IDLE";
}

