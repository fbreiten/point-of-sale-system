#ifndef REST_H
#define REST_H

#include <QObject>
#include <QUrl>
#include <QString>
#include <QtNetwork>

class rest : public QObject
{
    Q_OBJECT
public:
    explicit rest(QObject *parent = 0);
    ~rest();
    void post(QUrl url, QString message);
    void get(QUrl url);
    QString getMessage();
    
private:
    QNetworkAccessManager *m_pmanager;
    QString m_recievedMessage;

signals:
    void replied(QByteArray string);
    
private slots:
    void getReply(QNetworkReply *reply);
};

#endif // REST_H
