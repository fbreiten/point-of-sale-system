#include "cashbox.h"
#include "rest.h"
#include <QMessageBox>

CashBox::CashBox(ModelManager *manager, QObject *parent) :
    m_pmanager(manager), QObject(parent)
{
    m_pcashboxConnection = new rest();

    QObject::connect(m_pcashboxConnection, SIGNAL(replied(QByteArray)),
            this, SLOT(setStatus(QByteArray)));

    m_openUrl = QUrl("http://localhost:9001/cashbox/open");
    m_statusUrl = QUrl("http://localhost:9001/cashbox/status");
    m_ack = false;
    m_status = false;

}

CashBox::~CashBox()
{
    delete m_pcashboxConnection;
}

void CashBox::poll()
{
    m_pcashboxConnection->get(m_statusUrl);
}

//Opens the cashbox
void CashBox::open(QString amount)
{
    ListModel *model = m_pmanager->getSaleModel();
    if (model->rowCount() != 0) {
        if(amount.isEmpty()){
            QMessageBox::information(0, "No cash amount", "Enter a cash amount");
        }
        else {
            m_pcashboxConnection->post(m_openUrl,"");
            m_cashAmount = amount.toFloat();
        }
    }
}

void CashBox::setStatus(QByteArray status)
{
    if(QString(status) == "OPEN" && !m_ack)
    {
        m_ack = true;
        m_pmanager->prepareFinalize(m_cashAmount);
        getReturnAmount();
        m_status = true;
    }
    else if (QString(status) == "CLOSED")
    {
        m_status = false;
        m_ack = false;
    }
    emit updateStatus();
}

void CashBox::getReturnAmount() {

    ListModel *model = m_pmanager->getSaleModel();
    qreal returnAmount = m_pmanager->getAmountPaid() - model->getSum().toFloat();

    if (returnAmount > 0) {
        QMessageBox::information(0, "Amount to return", "Return " + QString::number(returnAmount, 'f', 2));
    }
}
