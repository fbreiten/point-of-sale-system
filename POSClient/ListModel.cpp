#include "listmodel.h"
#include "listproduct.h"
#include <QtCore>

void ListModel::emitSignal(QModelIndex index) {
    emit dataChanged(index, index);
}

ListModel::ListModel(ListItem* prototype, QObject *parent) :
    QAbstractListModel(parent), m_prototype(prototype)
{
  setRoleNames(m_prototype->roleNames());
  m_sum = "0";
}

int ListModel::rowCount(const QModelIndex &parent) const
{
  Q_UNUSED(parent);
  return m_list.size();
}

QVariant ListModel::data(const QModelIndex &index, int role) const
{
  if(index.row() < 0 || index.row() >= m_list.size())
    return QVariant();
  return m_list.at(index.row())->data(role);
}

ListModel::~ListModel() {
  delete m_prototype;
  clear();
}

void ListModel::appendRow(ListItem *item)
{
  appendRows(QList<ListItem*>() << item);
  calcSum();
  m_current = item;
  m_currentIndex = m_list.size() - 1;
  emit currentChanged();
}

void ListModel::appendRows(const QList<ListItem *> &items)
{
  beginInsertRows(QModelIndex(), rowCount(), rowCount()+items.size()-1);
  foreach(ListItem *item, items)
  {
    connect(item, SIGNAL(dataChanged()), SLOT(handleItemChange()));
    m_list.append(item);
  }
  endInsertRows();
}

void ListModel::insertRow(int row, ListItem *item)
{
  beginInsertRows(QModelIndex(), row, row);
  connect(item, SIGNAL(dataChanged()), SLOT(handleItemChange()));
  m_list.insert(row, item);
  endInsertRows();
}

void ListModel::handleItemChange()
{
  ListItem* item = static_cast<ListItem*>(sender());
  QModelIndex index = indexFromItem(item);
  if(index.isValid())
    emit dataChanged(index, index);
}

ListItem * ListModel::find(const qint64 &id)
{
  foreach(ListItem* item, m_list) {
    if(item->id() == id) return item;
  }
  return 0;
}

QModelIndex ListModel::indexFromItem(const ListItem *item) const
{
  Q_ASSERT(item);
  for(int row=0; row<m_list.size(); ++row) {
    if(m_list.at(row) == item) return index(row);
  }
  return QModelIndex();
}

void ListModel::clear()
{
  qDeleteAll(m_list);
  m_list.clear();
}

bool ListModel::removeRow(int row, const QModelIndex &parent)
{
  Q_UNUSED(parent);
  if(row < 0 || row >= m_list.size()) return false;
  beginRemoveRows(QModelIndex(), row, row);
  delete m_list.takeAt(row);
  endRemoveRows();
  calcSum();
  m_currentIndex = m_list.size() - 1;
  emit currentChanged();
  return true;
}

bool ListModel::removeRows(int row, int count, const QModelIndex &parent)
{
  Q_UNUSED(parent);
  if(row < 0 || (row+count) >= m_list.size()) return false;
  beginRemoveRows(QModelIndex(), row, row+count-1);
  for(int i=0; i<count; ++i) {
    delete m_list.takeAt(row);
  }
  endRemoveRows();
  return true;
}

ListItem * ListModel::takeRow(int row)
{
  beginRemoveRows(QModelIndex(), row, row);
  ListItem* item = m_list.takeAt(row);
  endRemoveRows();
  return item;
}

ListItem * ListModel::getItemAtIndex(int index) {

    if (m_list.size() == 0) return 0;
    else {
        ListItem* item = m_list.at(index);
        return item;
    }
}

void ListModel::calcSum() {

    qreal sum = 0;

    foreach (ListItem* product, m_list) {
        ListProduct* casted = static_cast<ListProduct*>(product);
        float discount = 1;
        if (casted->discount() != 0) {
            discount = 1 - casted->discount()/100;
        }
        sum += casted->price() * casted->quantity() * discount;
    }
    m_sum = QString::number(sum, 'f', 2);
    emit sumChanged();
}

bool ListModel::emptyList() {

    if(m_list.size() == 0) return false;
    int size = m_list.size();
    beginRemoveRows(QModelIndex(), 0, size - 1);
    for(int i=0; i < size; ++i) {
      delete m_list.takeAt(0);
    }
    endRemoveRows();
    m_sum = "";
    emit sumChanged();
    m_currentIndex = 0;
    emit currentChanged();
    return true;
}

void ListModel::resetModel() {
    int size = m_list.size();
    if (size > 0) {
        beginRemoveRows(QModelIndex(), 0, size - 1);
        endRemoveRows();
    }
    m_currentIndex = 0;
    emit currentChanged();
}

void ListModel::updateModel() {

    foreach(ListItem *item, m_list)
    {
      connect(item, SIGNAL(dataChanged()), SLOT(handleItemChange()));
    }
    calcSum();
}
