#ifndef MODELMANAGER_H
#define MODELMANAGER_H

#include <QtCore>
#include <QDebug>

#include "listmodel.h"
#include "listproduct.h"
#include "listhold.h"
#include "dbhandler.h"
#include "receiptcreator.h"
#include "customer.h"

class ModelManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ getName WRITE setName NOTIFY currentChanged)
    Q_PROPERTY(QString price READ getPrice WRITE setPrice NOTIFY currentChanged)
    Q_PROPERTY(QString remaining READ getRemaining NOTIFY remainingUpdated)
    Q_PROPERTY(qreal discount READ getDiscount NOTIFY currentChanged)

private:
    ListModel *m_pSaleModel;
    ListModel *m_pHoldModel;
    QString m_currentName;
    qreal m_currentPrice;
    qreal m_currentDiscount;
    DBHandler *m_pdb;
    Customer *m_pCustomer;

    QString m_bonusCard;
    QString m_paymentCard;
    QString m_goodThroughMonth;
    QString m_goodThroughYear;

    qreal m_amountPaid;
    qreal m_remaining;
    ReceiptCreator *m_pReceipt;
    qint8 m_holdID;
    bool m_changeColor;
    void finalizeSale();

public:
    explicit ModelManager(QObject *parent = 0, DBHandler *db = 0);

    Q_INVOKABLE void addProductById(const QString &id);
    Q_INVOKABLE void removeProduct(int index);
    Q_INVOKABLE void clearList();
    Q_INVOKABLE void incrementQuantity(int index, int amount);
    Q_INVOKABLE void decrementQuantity(int index);

    Q_INVOKABLE void setDiscount(int index, qreal percent);

    Q_INVOKABLE void changeList();
    Q_INVOKABLE inline void resetRemaining(){m_remaining = 0; emit remainingUpdated();}
    Q_INVOKABLE inline QString getRemaining(){return QString::number(m_remaining, 'f', 2); qDebug() << QString::number(m_remaining, 'f', 2);}
    Q_INVOKABLE inline qreal getDiscount(){return m_currentDiscount;}
    Q_INVOKABLE void holdSale();
    Q_INVOKABLE void restoreSale(int index);
    Q_INVOKABLE inline void printReciept() { m_pReceipt->printReceipt(); }
    Q_INVOKABLE inline qreal getAmountPaid() { return m_amountPaid; }
    Q_INVOKABLE inline void setAmountPaidToZero() { m_amountPaid = 0; }

    inline QString getName() { return m_currentName; }
    inline void setName(QString name) { m_currentName = name; }
    inline QString getPrice() { return QString::number(m_currentPrice * (1 - m_currentDiscount*0.01), 'f', 2); }
    inline void setPrice(QString price) { m_currentPrice = price.toFloat(); }

    inline ListModel* getSaleModel() { return m_pSaleModel; }
    inline ListModel* getHoldModel() { return m_pHoldModel; }

    inline void setBonus(QString nr) { m_bonusCard = nr; }
    inline void setPayment(QString nr) { m_paymentCard = nr; }
    inline void setGoodThroughYear(QString str) { m_goodThroughYear = str; }
    inline void setGoodThroughMonth(QString str) { m_goodThroughMonth = str; }
    inline int getListSize() {return m_pSaleModel->getList().size(); }

    void prepareFinalize(qreal amount);

signals:
    void currentChanged();
    void remainingUpdated();

public slots:
    bool addSaleToDatabase();
    void calcRemaining();
};

#endif // MODELMANAGER_H
