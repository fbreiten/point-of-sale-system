#include "listhold.h"

ListHold::ListHold(qint64 id, const QString &name, QList<ListItem *> list, QObject *parent) :
    ListItem(parent), m_id(id), m_name(name), m_pList(list)
{
}

QHash<int, QByteArray> ListHold::roleNames() const
{
  QHash<int, QByteArray> names;
  names[NameRole] = "name";
  names[IDRole] = "id";
  return names;
}

QVariant ListHold::data(int role) const
{
  switch(role) {
  case NameRole:
    return name();
  case IDRole:
    return id();
  default:
    return QVariant();
  }
}
