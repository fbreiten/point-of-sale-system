#ifndef CARDREADER_H
#define CARDREADER_H

#include <QObject>
#include "rest.h"
#include <QtCore>
#include "modelmanager.h"

class CardReader : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString state READ getState NOTIFY stateUpdate)
    Q_PROPERTY(QString status READ getStatus NOTIFY statusUpdate)

public:
    explicit CardReader(ModelManager *manager, QObject *parent = 0);
    ~CardReader();
    Q_INVOKABLE void poll();
    Q_INVOKABLE inline QString getState(){ return m_state; }
    Q_INVOKABLE inline QString getStatus(){ return m_status; }
    Q_INVOKABLE void begin(QString amount);
    Q_INVOKABLE void reset();
private:
    rest *m_pCardReader;
    rest *m_pCardReaderXML;
    QString m_state;
    QString m_status;
    ModelManager *m_pmanager;
    bool m_ack;
    qreal m_cardAmount;

signals:
    void stateUpdate();
    void statusUpdate();

public slots:
    void setStatus(QByteArray array);
    void setResult(QByteArray array);
};

#endif // CARDREADER_H
