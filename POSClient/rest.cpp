#include "rest.h"
#include <QString>
#include <QUrl>
#include <QtNetwork>

rest::rest( QObject *parent) :
    QObject(parent)
{
    m_pmanager = new QNetworkAccessManager(this);
    connect(m_pmanager, SIGNAL(finished(QNetworkReply *)),
                     SLOT(getReply(QNetworkReply*)));
}

rest::~rest()
{
    delete m_pmanager;
}

//post takes the message in type = value format as stated it the header
void rest::post(QUrl url, QString message)
{
    QNetworkRequest request = QNetworkRequest();

    request.setUrl(url);
    request.setRawHeader("Accept", "application/xml");
    request.setRawHeader("Content-Type", "application/x-www-form-urlencoded");
    m_pmanager->post(request, message.toAscii());
}

//slot for retrieving the newly downloaded message, the message will be a QByteArray
//connect to replied signal to get the message from the rest server
void rest::getReply(QNetworkReply *reply)
{
    if(reply->error() > 0) {
        qDebug() << "REST error: " << reply->errorString();
        emit replied(reply->readAll());
    } else {
        emit replied(reply->readAll());
    }
    reply->deleteLater();
}

//retreives in xml format needs to be parsed with QXml parser, see cardreader for examples.
void rest::get(QUrl url)
{
    QNetworkRequest request = QNetworkRequest();
    request.setUrl(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "text/xml");
    QNetworkReply *reply = 0;
    reply = m_pmanager->get(request);
}
