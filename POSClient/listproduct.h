#ifndef LISTPRODUCT_H
#define LISTPRODUCT_H
#include "listmodel.h"
#include <QtCore>

class ListProduct : public ListItem
{
  Q_OBJECT

public:
  enum Roles {
    NameRole = Qt::UserRole+1,
    QuantityRole,
    PriceRole,
    IDRole,
    TotalRole,
    DiscountRole
  };

public:
  ListProduct(QObject *parent = 0): ListItem(parent) {}
  explicit ListProduct(const qint64 &id, const QString &name, int quantity, qreal price, qreal discount, QObject *parent = 0);
  QVariant data(int role) const;
  QHash<int, QByteArray> roleNames() const;

  void setPrice(qreal price);
  void setDiscount(qreal discount);
  void setQuantity(qreal quantity);

  inline qint64 id() const { return m_id; }
  inline QString name() const { return m_name; }
  inline int quantity() const { return m_quantity; }
  inline qreal price() const { return m_price; }
  inline QString total() const { return m_total; }
  inline qreal discount() const { return m_discount; }

private:
  QString m_name;
  int m_quantity;
  qreal m_price;
  qint64 m_id;
  QString m_total;
  qreal m_discount;
  void updateTotal();
};
#endif
