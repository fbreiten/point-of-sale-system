#ifndef CASHBOX_H
#define CASHBOX_H

#include <QObject>
#include "rest.h"
#include "modelmanager.h"

class CashBox : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool status READ getStatus NOTIFY updateStatus)

public:
    explicit CashBox(ModelManager *manager, QObject *parent = 0);
    ~CashBox();

    Q_INVOKABLE inline bool getStatus() const { return m_status; }
    Q_INVOKABLE void open(QString amount);
    Q_INVOKABLE void poll();

    void getReturnAmount();

private:
    ModelManager *m_pmanager;
    rest *m_pcashboxConnection;
    QUrl m_openUrl;
    QUrl m_statusUrl;
    qreal m_cashAmount;
    bool m_ack;
    bool m_status;
    
signals:
    void updateStatus();

public slots:
    void setStatus(QByteArray status);
};

#endif // CASHBOX_H
