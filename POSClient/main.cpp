#include <QApplication>
#include <QtCore>
#include <QDeclarativeContext>
#include "qmlapplicationviewer.h"
#include <QMessageBox>

#include "listmodel.h"
#include "listproduct.h"
#include "modelmanager.h"
#include "dbhandler.h"
#include "cashbox.h"
#include "cardreader.h"
#include <QtTest/QtTest>

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QScopedPointer<QApplication> app(createApplication(argc, argv));
    QmlApplicationViewer cashierView;

    DBHandler db;
    db.createConnection();

    ModelManager manager(0, &db);
    CardReader cardReader(&manager);
    CashBox cashbox(&manager);

    cashierView.rootContext()->setContextProperty("cardReader", &cardReader);
    cashierView.rootContext()->setContextProperty("cashBox", &cashbox);
    cashierView.rootContext()->setContextProperty("listModel", manager.getSaleModel());
    cashierView.rootContext()->setContextProperty("holdModel", manager.getHoldModel());
    cashierView.rootContext()->setContextProperty("modelManager", &manager);
    cashierView.setWindowTitle("Cashier Window");
    cashierView.setMainQmlFile(QLatin1String("qml/POSClient/CashierView.qml"));
    cashierView.showExpanded();

    QmlApplicationViewer customerView;

    customerView.rootContext()->setContextProperty("listModel", manager.getSaleModel());
    customerView.rootContext()->setContextProperty("modelManager", &manager);
    customerView.rootContext()->setContextProperty("listModel", manager.getSaleModel());
    customerView.setMainQmlFile(QLatin1String("qml/POSClient/CustomerView.qml"));
    customerView.setWindowTitle("Customer Window");
    customerView.showExpanded();

    return app->exec();
}

