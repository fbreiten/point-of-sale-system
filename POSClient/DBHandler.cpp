#include "dbhandler.h"
#include "modelmanager.h"
#include <QMessageBox>
#include "listmodel.h"

DBHandler::DBHandler(QObject *parent) :
    QObject(parent)
{
    m_customerID = 0;
}

DBHandler::~DBHandler()
{
    m_db.close();
}

void DBHandler::createConnection()
{
    m_db = QSqlDatabase::addDatabase("QMYSQL");
    m_db.setHostName("localhost");
    m_db.setDatabaseName("posdb");
    m_db.setUserName("root");
    m_db.setPassword("root");
    qDebug() << "Opening database...";
    if (!m_db.open())
    {
        qDebug() << "Could not open database: " << m_db.lastError().text();
    } else
    {
        qDebug() << "Connected";
    }
}

bool DBHandler::retrieveProduct(qint64 id)
{
    QSqlQuery query("SELECT name, price, discount FROM product where product_id = ' ? '");
    query.bindValue(0, id);
    query.exec();
    if (query.next()) {
        m_name = query.value(0).toString();
        m_price = query.value(1).toFloat();
        m_discount = query.value(2).toFloat();
        if (m_discount == NULL) {
            m_discount = 0;
        }
        return true;
    }
    else {
        QMessageBox::information(0, "Try again", "Product not found");
        return false;
    }
}

bool DBHandler::retrieveSaleID() {
    QSqlQuery query("SELECT MAX(sale_id) FROM sale_entity");
    query.exec();
    if (query.next()) {
        m_saleID = query.value(0).toInt();
        return true;
    }
    else {
        qDebug() << "Failed to retrieve latest sale id";
        return false;
    }
}

bool DBHandler::retrieveCustomerIDWithBonusID(QString card) {
    
    QSqlQuery query;
    query.prepare("SELECT customer_id FROM bonus_card WHERE bonus_card_nr = :card");
    query.bindValue(":card", card);
    query.exec();

    qDebug() << "retrieving Customer ID: " << query.lastError().text();

    if (query.next()) {
        m_customerID = query.value(0).toInt();
        m_isBonusCustomer = true;
        return true;
    }
    else {
        qDebug() << "Failed to fetch customer id with bonus card number, reason: " << query.lastError().text();
        return false;
    }
}

bool DBHandler::writeSaleData(ListProduct* product) {
    
    QSqlQuery query;
    query.prepare("INSERT INTO sale_data (sale_id, product_id, product_quantity) VALUES(:saleid, :productid, :quantity)");
    query.bindValue(":saleid", m_saleID);
    query.bindValue(":productid", product->id());
    query.bindValue(":quantity", product->quantity());
    bool noError = query.exec();
    if(noError)
    {
        return true;
    } else
    {
        qDebug() << "Error writing sale Data: " << query.lastError().text();
        return false;
    }
}

bool DBHandler::writeSaleEntity(ListModel *model) {
    
    float bonusPoints = 0;

    QSqlQuery query;

    query.prepare("INSERT INTO sale_entity (total_sum, customer_id, sale_date, bonus_points) VALUES(:sum, :custid, :date, :bonus)");
    query.bindValue(":sum", model->getSum());
    query.bindValue(":custid", m_customerID);
    query.bindValue(":date", QDateTime::currentDateTime());

    if (m_isBonusCustomer)
    {
        bonusPoints = model->getSum().toFloat()*0.05;
    }
    query.bindValue(":bonus", bonusPoints);
    bool noError = query.exec();
    m_customerID = 0;
    if(noError)
    {
        return true;
    } else
    {
        qDebug() << "Error writing sale entity: " << query.lastError().text();
        return false;
    }
}

bool DBHandler::writeCustomerData(Customer *customer)
{
    qDebug() << "customer id: " << customer->getcustomerId();
    if(!customer->getcustomerId().isEmpty())
    {
        QSqlQuery query;
        query.prepare("INSERT INTO customer "
                      "(customer_id, first_name, last_name, sex, birth_date, address, postal_code, post_office, country)"
                      " VALUES(:customer_id,:first_name, :last_name, :sex, :birth_date, :address, :postal_code, :post_office, :country)");
        query.bindValue(":customer_id",customer->getcustomerId());
        query.bindValue(":first_name",customer->getfirstName());
        query.bindValue(":last_name", customer->getlastName());
        query.bindValue(":sex", customer->getsex());
        query.bindValue(":birth_date",customer->getbirthDate());
        query.bindValue(":address", customer->getaddress());
        query.bindValue(":postal_code", customer->getpostalCode());
        query.bindValue(":post_office",customer->getpostOffice());
        query.bindValue(":country", customer->getcountry());
        bool noError = query.exec();
        writeBonusCardData(customer);
        emit customerDataUpdated();

        if(noError)
        {
            return true;
        } else
        {
            if(query.lastError().text().contains("Duplicate"))
            {
                qDebug() << "Customer already exists, nothing to see here...";
            } else
            {
                qDebug() << "Error inserting customer data: " << query.lastError().text();
            }
            return false;
        }
    }

    emit customerDataUpdated();
    return true;
}

bool DBHandler::writeBonusCardData(Customer *customer)
{
    QSqlQuery query;
    query.prepare("INSERT INTO bonus_card "
                  "(customer_id, blocked, expired, good_through_month, good_through_year, bonus_card_nr)"
                  " VALUES(:customer_id, :blocked, :expired, :good_through_month, :good_through_year, :bonus_card_nr)");
    query.bindValue(":customer_id", customer->getcustomerId());
    query.bindValue(":blocked", customer->getblocked());
    query.bindValue(":expired", customer->getexpired());
    query.bindValue(":good_through_month",customer->getgoodThroughMonth());
    query.bindValue(":good_through_year", customer->getgoodThroughYear());
    query.bindValue(":bonus_card_nr", customer->getbonusCardNumber());
    bool noError = query.exec();

    if(noError)
    {
        return true;
    } else
    {
        if(query.lastError().text().contains("Duplicate"))
        {
            qDebug() << "Bonus card already exists, nothing to see here...";
        } else
        {
            qDebug() << "Error inserting customer data: " << query.lastError().text();
        }
        return false;
    }
}
