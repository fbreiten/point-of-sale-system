#ifndef LISTHOLD_H
#define LISTHOLD_H

#include "listmodel.h"
#include <QTime>
#include <QtCore>

class ListHold : public ListItem
{
    Q_OBJECT
public:
  enum Roles {
    NameRole = Qt::UserRole+1,
    IDRole
  };
public:
    ListHold(QObject *parent = 0): ListItem(parent) {}
    explicit ListHold(qint64 id, const QString &name, QList<ListItem*> list, QObject *parent);
    QVariant data(int role) const;
    QHash<int, QByteArray> roleNames() const;

    inline QString name() const { return m_name; }
    inline qint64 id() const { return m_id; }
    inline QList<ListItem*> getList() { return m_pList; }

private:
    QList<ListItem*> m_pList;
    QString m_name;
    qint64 m_id;
};

#endif // LISTHOLD_H
