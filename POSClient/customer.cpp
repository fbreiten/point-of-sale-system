#include "customer.h"
#include <QDomDocument>
#include <QXmlInputSource>

Customer::Customer(QObject *parent) :
    QObject(parent)
{
    m_pCustomerInformation = new rest();
    QObject::connect(m_pCustomerInformation, SIGNAL(replied(QByteArray)),
            this, SLOT(setInformation(QByteArray)) );
}

Customer::~Customer()
{
    delete m_pCustomerInformation;
}

void Customer::setInformation(QByteArray array)
{
    QDomDocument doc;
    QXmlInputSource is;
    QDomElement element;
    is.setData(array);
    doc.setContent(is.data());
    element = doc.documentElement();

    QDomNodeList customerList = doc.elementsByTagName("firstName");
    m_firstName = customerList.at(0).toElement().text();

    m_customerId = element.attribute("customerNo");
    qDebug() << "customer id: " << m_customerId;

    customerList = doc.elementsByTagName("lastName");
    m_lastName = customerList.at(0).toElement().text();

    customerList = doc.elementsByTagName("birthDate");
    m_birthDate = customerList.at(0).toElement().text();
    qDebug() << "before " << m_birthDate;
    m_birthDate.resize(m_birthDate.indexOf('T'));
    qDebug() << "after " << m_birthDate;

    customerList = doc.elementsByTagName("streetAddress");
    m_address = customerList.at(0).toElement().text();

    customerList = doc.elementsByTagName("postalCode");
    m_postalCode = customerList.at(0).toElement().text();

    customerList = doc.elementsByTagName("postOffice");
    m_postOffice = customerList.at(0).toElement().text();

    customerList = doc.elementsByTagName("country");
    m_country = customerList.at(0).toElement().text();

    customerList = doc.elementsByTagName("number");
    m_bonusCardNumber = customerList.at(0).toElement().text();

    customerList = doc.elementsByTagName("goodThruMonth");
    m_goodThroughMonth = customerList.at(0).toElement().text();

    customerList = doc.elementsByTagName("goodThruYear");
    m_goodThroughYear = customerList.at(0).toElement().text();

    customerList = doc.elementsByTagName("blocked");
    m_blocked = customerList.at(0).toElement().text();

    customerList = doc.elementsByTagName("expired");
    m_expired = customerList.at(0).toElement().text();

    customerList = doc.elementsByTagName("holderName");
    m_holderName = customerList.at(0).toElement().text();

    customerList = doc.elementsByTagName("sex");
    m_sex = customerList.at(0).toElement().text();

    emit informationUpdated(this);
}

void Customer::retrieveInformation(QString number, QString year, QString month)
{
    QString message = number + "/" + year + "/" + month;
    qDebug() << message;
    m_pCustomerInformation->get(QUrl("http://localhost:9004/rest/findByBonusCard/" + message));
}
