/*The Listproducts are the items that will show up in the GUI
 *NOTE: the price is already calculated in the Constructor
 */

#include "listproduct.h"
#include <QtCore>

ListProduct::ListProduct(const qint64 &id, const QString &name, int quantity, qreal price, qreal discount, QObject *parent) :
    ListItem(parent), m_id(id), m_name(name), m_quantity(quantity), m_price(price),
    m_total(QString::number(price*(1 - discount*0.01), 'f', 2)), m_discount(discount)
{
    if (m_discount == 0)
    {
        m_total = QString::number(m_price*m_quantity, 'f', 2);
    }
}

void ListProduct::setQuantity(qreal quantity)
{
    m_quantity = quantity;
    updateTotal();
    emit dataChanged();
}

void ListProduct::setPrice(qreal price)
{
    if(m_price != price) {
        m_price = price;
        updateTotal();
        emit dataChanged();
  }
}

void ListProduct::setDiscount(qreal discount)
{
        if(m_discount != discount) {
        m_discount = discount;
        updateTotal();
        emit dataChanged();
    }
}

QHash<int, QByteArray> ListProduct::roleNames() const
{
  QHash<int, QByteArray> names;
  names[NameRole] = "name";
  names[QuantityRole] = "quantity";
  names[PriceRole] = "price";
  names[TotalRole] = "total";
  names[DiscountRole] = "discount";
  return names;
}

QVariant ListProduct::data(int role) const
{
  switch(role) {
  case NameRole:
    return name();
  case QuantityRole:
    return quantity();
  case PriceRole:
    return price();
  case TotalRole:
    return total();
  case DiscountRole:
    return discount();
  default:
    return QVariant();
  }
}

void ListProduct::updateTotal()
{
    float price = m_price*m_quantity;
    if (m_discount != 0) {
        price = m_price*m_quantity*(1 - m_discount/100);
    }
    m_total = QString::number(price, 'f', 2);
    qDebug() << "updated";
}
