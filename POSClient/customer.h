#ifndef CUSTOMER_H
#define CUSTOMER_H

#include <QObject>
#include "rest.h"
#include <QtCore>

class Customer : public QObject
{
    Q_OBJECT
public:
    explicit Customer(QObject *parent = 0);
    ~Customer();

    void retrieveInformation(QString number, QString year, QString month);
    inline QString getcustomerId(){return m_customerId;}
    inline QString getfirstName(){return m_firstName;}
    inline QString getlastName(){return m_lastName;}
    inline QString getbirthDate(){return m_birthDate;}
    inline QString getbonusCardNumber(){return m_bonusCardNumber;}
    inline QString getgoodThroughMonth(){return m_goodThroughMonth;}
    inline QString getgoodThroughYear(){return m_goodThroughYear;}
    inline QString getblocked(){return m_blocked;}
    inline QString getexpired(){return m_expired;}
    inline QString getholderName(){return m_holderName;}
    inline QString getsex(){return m_sex;}

    inline QString getaddress(){return m_address;}
    inline QString getpostalCode(){return m_postalCode;}
    inline QString getpostOffice(){return m_postOffice;}
    inline QString getcountry(){return m_country;}

private:
    rest *m_pCustomerInformation;

    QString m_customerId;
    QString m_firstName;
    QString m_lastName;
    QString m_birthDate;
    QString m_bonusCardNumber;
    QString m_goodThroughMonth;
    QString m_goodThroughYear;
    QString m_blocked;
    QString m_expired;
    QString m_holderName;
    QString m_sex;

    QString m_address;
    QString m_postalCode;
    QString m_postOffice;
    QString m_country;

signals:
    void informationUpdated(Customer* customer);
    void informationSavedToDatabase();
public slots:
    void setInformation(QByteArray array);
};

#endif // CUSTOMER_H
