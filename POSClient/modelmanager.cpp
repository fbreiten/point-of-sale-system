#include "modelmanager.h"
#include "listproduct.h"
#include <math.h>
#include <QtDebug>

ModelManager::ModelManager(QObject *parent, DBHandler *db) :
    QObject(parent)
{
    m_pSaleModel = new ListModel(new ListProduct, qApp);
    m_pHoldModel = new ListModel(new ListHold, qApp);
    m_pReceipt = new ReceiptCreator;
    m_currentName = "";
    m_currentPrice = 0;
    m_currentDiscount = 1;
    m_pdb = db;
    m_pCustomer = new Customer();

    m_bonusCard = "";
    m_paymentCard = "";
    m_amountPaid = 0;
    m_holdID = 1;

    //Sends a signal when the rest information has been saved
    QObject::connect(m_pCustomer, SIGNAL(informationUpdated(Customer*)),
            m_pdb, SLOT(writeCustomerData(Customer*)) );

    //Sends a information when the Customer information has been added to the database
    QObject::connect(m_pdb, SIGNAL(customerDataUpdated()),
           this , SLOT(addSaleToDatabase()) );

    //Recalculates the remaining sum whenever data is changed
    QObject::connect(this, SIGNAL(currentChanged()), SLOT(calcRemaining()));
}

void ModelManager::addProductById(const QString &id) {

    qDebug() << "Adding product with id = " + id;
    int idNum = id.toInt();
    ListItem* item = m_pSaleModel->find(idNum);
    QString name = "";
    qreal price = 0;
    qreal discount = 1;

    if (item == 0) {

        //Queried product data is stored in DBHandlers members
        if (m_pdb->retrieveProduct(idNum)) {
            name = m_pdb->getName();
            price = m_pdb->getPrice();
            price = floor(100*price + 0.5) / 100; //This rounds of the digits
            discount = m_pdb->getDiscount();
            qDebug() << "price: " << price;
            discount = floor(100*discount + 0.5) / 100; //This rounds of the digits

            ListProduct* temp = new ListProduct(idNum, name, 1, price, discount, m_pSaleModel);

            m_pSaleModel->appendRow(temp);
            m_currentName = name;
            m_currentPrice = price;
            m_currentDiscount = discount;
        }
    }
    else {

        ListProduct* casted = static_cast<ListProduct*>(item);
        int quantity = casted->quantity();
        casted->setQuantity(++quantity);
        m_currentPrice = casted->price();
        QModelIndex emitIndex = m_pSaleModel->indexFromItem(item);
        m_pSaleModel->emitSignal(emitIndex);
        m_pSaleModel->calcSum();
        m_currentName = casted->name();
        qDebug() << casted->name();
    }
    emit currentChanged();

}

void ModelManager::incrementQuantity(int index, int amount) {

    ListItem* item = m_pSaleModel->getItemAtIndex(index);

    if (item != 0)
    {
        ListProduct* casted = static_cast<ListProduct*>(item);

        int quantity = casted->quantity();
        int limit = quantity + amount;
        while (quantity < limit) {
            ++quantity;
        }

        casted->setQuantity(quantity);
        m_currentPrice = casted->price();

        QModelIndex emitIndex = m_pSaleModel->indexFromItem(item);
        m_pSaleModel->emitSignal(emitIndex);
        m_pSaleModel->calcSum();

        m_currentName = casted->name();
        qDebug() << casted->name();
        emit currentChanged();
    }
}

void ModelManager::decrementQuantity(int index) {

    ListItem* item = m_pSaleModel->getItemAtIndex(index);

    if (item != 0)
    {
        ListProduct* casted = static_cast<ListProduct*>(item);
        int quantity = casted->quantity();
        if (quantity == 1) {
            removeProduct(index);
        }
        else {
            int quantity = casted->quantity();
            casted->setQuantity(--quantity);

            QModelIndex emitIndex = m_pSaleModel->indexFromItem(item);
            m_pSaleModel->emitSignal(emitIndex);
            m_pSaleModel->calcSum();
        }
    }
    emit currentChanged();
}

void ModelManager::setDiscount(int index, qreal percent)
{
    ListItem* item = m_pSaleModel->getItemAtIndex(index);

    if(item !=0 )
    {
        ListProduct* casted = static_cast<ListProduct*>(item);
        casted->setDiscount(percent);
        m_currentDiscount = percent;
        m_pSaleModel->calcSum();
        emit currentChanged();
    }
}

void ModelManager::removeProduct(int index) {

    m_pSaleModel->removeRow(index);
    m_currentName = "";
    m_currentPrice = 0;
    emit currentChanged();
}

void ModelManager::clearList() {

    m_pSaleModel->emptyList();
    m_currentName = "";
    m_currentPrice = 0;
    emit currentChanged();
}

void ModelManager::finalizeSale() {

    qDebug() << "Bonus card number: " << m_bonusCard;
    if (m_bonusCard != "") {
        m_pCustomer->retrieveInformation(m_bonusCard, m_goodThroughYear, m_goodThroughMonth);
    } else
    {
        addSaleToDatabase();
    }
}

bool ModelManager::addSaleToDatabase()
{
    m_pdb->retrieveCustomerIDWithBonusID(QString(m_bonusCard));

    bool noError = m_pdb->writeSaleEntity(m_pSaleModel);
    m_pdb->retrieveSaleID();

    QList<ListItem*> productList = m_pSaleModel->getList();
    m_pReceipt->setData(productList.size(), m_pSaleModel->getSum());
    foreach (ListItem* item, productList) {
        ListProduct* product = static_cast<ListProduct*>(item);
        noError &= m_pdb->writeSaleData(product);
        m_pReceipt->addRecord(product);
    }
    if(noError)
    {
        qDebug() << "Sale completed";
        return true;
    } else
    {
        return false;
    }
}

void ModelManager::calcRemaining()
{
    m_remaining = m_pSaleModel->getSum().toFloat() - m_amountPaid;
    qDebug() << m_remaining << " remaining";
    emit remainingUpdated();
}

void ModelManager::prepareFinalize(qreal amount) {

    m_amountPaid += amount;
    calcRemaining();
    if (m_remaining <= qreal(0)) {
        finalizeSale();
    } else {
        qDebug() << m_remaining << " remaining";
    }
}

void ModelManager::holdSale()
{
    if (m_pSaleModel->rowCount() > 0) {
        QList<ListItem*> list = *(new QList<ListItem*>);
        m_pHoldModel->appendRow(new ListHold(m_holdID, QTime::currentTime().toString(), m_pSaleModel->getList(), m_pHoldModel));
        m_pSaleModel->resetModel();
        m_pSaleModel->setList(list);

        m_pSaleModel->calcSum();
        m_holdID++;
    }

}

void ModelManager::restoreSale(int index)
{
    if (m_pHoldModel->rowCount() > 0) {
        ListHold* holdItem = static_cast<ListHold*>(m_pHoldModel->getItemAtIndex(index));

        m_pSaleModel->emptyList();
        m_pSaleModel->clear();

        QList<ListItem*> productList = holdItem->getList();
        foreach (ListItem *item , productList) {
            ListProduct* product = static_cast<ListProduct*>(item);
            m_pSaleModel->appendRow(product);
        }

        m_pHoldModel->removeRow(index);
        m_pSaleModel->updateModel();
    }
}

void ModelManager::changeList()
{
    m_pSaleModel->emptyList();
    m_pSaleModel->clear();
    QList<ListItem*> list;
    ListProduct* temp1 = new ListProduct(1, "name", 1, 100, 1, m_pSaleModel);
    ListProduct* temp2 = new ListProduct(2, "jonathan", 1, 100, 1, m_pSaleModel);
    list.append(temp1);
    list.append(temp2);
    m_pSaleModel->setList(list);
    m_pSaleModel->updateModel();
}
