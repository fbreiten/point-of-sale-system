#ifndef LISTMODEL_H
#define LISTMODEL_H
#include <QtCore>

class ListItem: public QObject {
  Q_OBJECT

public:
  ListItem(QObject* parent = 0) : QObject(parent) {}
  virtual ~ListItem() {}
  virtual qint64 id() const = 0;
  virtual QVariant data(int role) const = 0;
  virtual QHash<int, QByteArray> roleNames() const = 0;

signals:
  void dataChanged();
};

class ListModel : public QAbstractListModel
{
  Q_OBJECT
  Q_PROPERTY(QString sum READ getSum NOTIFY sumChanged)
  Q_PROPERTY(int current READ getCurrentIndex NOTIFY currentChanged)


public:
  explicit ListModel(ListItem* prototype, QObject* parent = 0);
  ~ListModel();
  Q_INVOKABLE int rowCount(const QModelIndex &parent = QModelIndex()) const;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
  void appendRow(ListItem* item);
  void appendRows(const QList<ListItem*> &items);
  void insertRow(int row, ListItem* item);
  Q_INVOKABLE bool removeRow(int row, const QModelIndex &parent = QModelIndex());
  bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());
  inline QList<ListItem*> getList() { return m_list; }
  ListItem* takeRow(int row);
  ListItem* find(const qint64 &id);
  QModelIndex indexFromItem( const ListItem* item) const;

  inline void setList(QList<ListItem*> list){ m_list = list; }

  ListItem * getItemAtIndex(int index);
  void clear();

  bool emptyList();
  inline QString getSum(){return m_sum;}
  inline int getCurrentIndex() { return m_currentIndex; }
  void calcSum();

  void emitSignal(QModelIndex index);
  void resetModel();
  void updateModel();

  QList<ListItem*> m_list;
private slots:
  void handleItemChange();

private:
  ListItem* m_prototype;
  QString m_sum;
  ListItem* m_current;
  int m_currentIndex;
signals:
  void sumChanged();
  void currentChanged();
};
#endif
