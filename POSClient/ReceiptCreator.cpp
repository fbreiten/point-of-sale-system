#include "receiptcreator.h"
#include <QtCore>
#include <QPainter>
#include <QDesktopServices>
#include <QUrl>


void ReceiptCreator::setData(int listSize, QString sum)
{
    m_sum = sum;
    m_position = 20;
    m_pPixmap = new QPixmap(320, listSize*20+120); //size in pixels
    m_pPainter = new QPainter(m_pPixmap);

    QColor white(255,255,255);
    m_pPixmap->fill(white);

    QFont font("", 10); //default font
    m_pPainter->setFont(font);
    addHeader();
}

void ReceiptCreator::printReceipt()
{
    if (m_sum != NULL)
    {
        m_pPainter->drawText(QPointF(10, m_position + 20), "Total:");
        m_pPainter->drawText(QPointF(260, m_position + 20), m_sum);
        m_position += 30;
        addFooter();
        m_pPixmap->save("receipt.jpg");
        QDesktopServices::openUrl(QUrl("receipt.jpg"));
        qDebug() << "Printed receipt";
    }
}

void ReceiptCreator::addRecord(ListProduct *product)
{
    m_pPainter->drawText(QPointF(10, m_position), product->name());
    float discount = product->discount();
    QString price = QString::number(product->price() * product->quantity() * (1-discount/100), 'f', 2);
    m_pPainter->drawText(260, m_position, price);

    m_position += 20;
}

void ReceiptCreator::addFooter()
{
    m_pPainter->setFont(QFont("",10));
    m_pPainter->drawText(QPointF(10, m_position + 10), "tel: +358 40 1234560");
    m_pPainter->drawText(QPointF(10, m_position + 22), "Have an awesome day!");
    m_pPainter->setFont(QFont("",10));
    m_position += 20;
}

void ReceiptCreator::addHeader()
{
    m_pPainter->setFont(QFont("",16));
    m_pPainter->drawText(QPointF(100, 30), "Omnistore");
    m_pPainter->setFont(QFont("",10));
    m_position += 30;
}
