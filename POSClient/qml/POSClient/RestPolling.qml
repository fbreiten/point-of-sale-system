import QtQuick 1.1

Item {
    Timer {
        interval: 500
        running: true
        repeat: true
        onTriggered: {
            cardReader.poll()
            cashBox.poll();

            if(cardReader.getStatus() === "WAITING_FOR_CARD")
                informationBar.state = "stage2loading"
            if((modelManager.getRemaining() <= 0.00) && (modelManager.getAmountPaid() !== 0.00)){
                informationBar.state = "stage3" // stage3 is done
                modelManager.setAmountPaidToZero()
                modelManager.clearList()
            }
            if(cardReader.state == "INVALID_PIN")
            {
              informationBar.status = "Invalid Pin"
                informationBar.state = "stage3failed"
            }
            else if(cardReader.state == "INSUFFICIENT_FUNDS")
            {
                      informationBar.state = "stage3failed"
                        informationBar.status = "Insufficient Funds"
            }
            else if(cardReader.state == "UNSUPPORTED_CARD")
            {
                      informationBar.status = "Unknown Bank Card"
                        informationBar.state = "stage3failed"
            }
            else if(cardReader.state == "bonusUNSUPPORTED_CARD")
            {
                      informationBar.status = "Unknown Bonus Card"
                        informationBar.state = "stage3failed"
            }
            else if(cardReader.state == "bonusACCEPTED")
            {
                      informationBar.status = "Accepted Bonus Card"
                        informationBar.state = "stage3bonus"
            }
            else if(cardReader.state == "NETWORK_ERROR")
            {
                      informationBar.status = "Network Error"
                        informationBar.state = "stage3failed"
            } else
            informationBar.status = "Waiting..."
        }
    }
}

