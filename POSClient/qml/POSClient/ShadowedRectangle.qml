import QtQuick 1.1

Item {
    property alias color : rectangle.color

    BorderImage {
        id: shadowimage
        source: "shadows.png"
        opacity: 0.05
        anchors.fill: rectangle
        anchors {
            leftMargin: -6
            topMargin: -6
            rightMargin: -8
            bottomMargin: -8
        }
        border {
            left: 10
            top: 10
            right: 10
            bottom: 10
        }

        smooth: true
    }
    Rectangle{
        id: rectangle
        anchors.fill: parent
    }
}
