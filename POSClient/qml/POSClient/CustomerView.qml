import QtQuick 1.1

Rectangle {

    id: window
    width: 400
    height: 100
    color: "#464646"

    Rectangle {
        id: sumbar
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        height: 30
        color: "#464646"
        Text {
            id: totLabel
            color: "#f1f1f1"
            text: "Total: " + listModel.sum + " €"
            anchors.right: parent.right
            anchors.rightMargin: 10
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: parent.horizontalAlignment
            font.bold: true
            font.pixelSize: 18
        }
    }


    Rectangle {
        id: rectangle1
        height: 64
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#6d6d6d"
            }

            GradientStop {
                position: 1
                color: "#555555"
            }
        }
        //border.color: "#000000"
        anchors.right: parent.right
        anchors.left: parent.left
        Text {
            width: 65
            height: 64
            id: nameLabel
            color: "#f1f1f1"
            text: modelManager.name
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.top: parent.top
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            font.pixelSize: 18
        }


        Text {
            id: sumLabel
            //x: 315
            //y: 0
            width: 65
            height: 64
            color: "#f1f1f1"
            text: modelManager.price + " €"
            anchors.right: parent.right
            anchors.rightMargin: 20
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            font.bold: true
            font.pixelSize: 18
        }
    }
}
