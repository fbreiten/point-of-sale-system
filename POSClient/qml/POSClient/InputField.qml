/* Simple InputField containing containing a Textinput
*  NOTE: Keys.onReturnPressed() only works with TextInput
*/
import QtQuick 1.1

Rectangle {
    width: 350
    height: parent.height
    property alias text: textin.text
    property alias disable: textin.readOnly
    property alias toggleFocus: textin.focus
    border.width: 2
    border.color: "grey"

    TextInput{
        id: textin
        anchors.centerIn: parent
        text: ""
        font.pointSize: 16
        activeFocusOnPress : true
        focus: true

    }
    MouseArea {
        anchors.fill: parent
        onClicked: textin.forceActiveFocus()
    }
}
