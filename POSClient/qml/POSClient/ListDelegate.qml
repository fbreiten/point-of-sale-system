import QtQuick 1.1

Component {
    id:wrapper
    Rectangle {
        id: rectangle1
        clip: true //This is so the text is always inside the box
        height: 64
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#6d6d6d"
            }

            GradientStop {
                position: 1
                color: "#555555"
            }
        }
        //Animations
        ListView.onAdd: SequentialAnimation {
                         PropertyAction { target: rectangle1; property: "height"; value: 0 }
                         NumberAnimation { target: rectangle1; property: "height"; to: 64; duration: 250; easing.type: Easing.InOutQuad }
                     }

        ListView.onRemove: SequentialAnimation {
                         PropertyAction { target: rectangle1; property: "ListView.delayRemove"; value: true }
                         NumberAnimation { target: rectangle1; property: "height"; to: 0; duration: 250; easing.type: Easing.InOutQuad }
                         PropertyAction { target: rectangle1; property: "ListView.delayRemove"; value: false}
        }

        MouseArea {
            anchors.fill: parent
            onClicked: list.currentIndex = index //Saves the the index of clicked item
        }

        border.color: "#000000"
        anchors.right: parent.right
        anchors.left: parent.left
        states: State {
                         name: "Current"
                         when: rectangle1.ListView.isCurrentItem
                         PropertyChanges { target: rectangle1; border.color: "lightsteelblue"; border.width:2; }
                     }
        Text {
            id: nameLabel
            color: "#f1f1f1"
            text: name
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.top: parent.top
            anchors.topMargin: 10
            font.pixelSize: 16
        }

        Text {
            id: priceLabel
            x: 83
            y: 40
            color: "#f1f1f1"
            text: price + " €"
            font.pixelSize: 12
        }

        Text {
            id: sumLabel
            x: 315
            y: 0
            width: 65
            height: 64
            color: "#f1f1f1"
            text: total + " €"
            anchors.right: parent.right
            anchors.rightMargin: 60
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            font.bold: true
            font.pixelSize: 18
        }

        Text {
            id: quantityLabel
            x: 13
            y: 40
            color: "#f1f1f1"
            text: quantity
            font.pixelSize: 12
        }

        Text {
            id: timesLabel
            x: 60
            y: 40
            color: "#f1f1f1"
            width: 23
            height: 14
            text: "x"
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 12
        }

        Text {
            id: discountLabel
            color: "#f1f1f1"
            font.bold: true
            x: 150
            y: 23
            font.pixelSize: 16
            text: if(discount != 0)
                    "-" + (discount) + "%"
            else
                      ""
            MouseArea {
                anchors.fill: parent; onClicked: modelManager.setDiscount(index, 0)
            }
        }
         Image {
             anchors.right: parent.right
             y: 8
             source: "minus-icon.png"
             MouseArea { anchors.fill:parent; onClicked: modelManager.removeProduct(index) }
         }
    }
}
