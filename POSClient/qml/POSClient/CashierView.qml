import QtQuick 1.1

Rectangle {
    id: window

    width: 1024
    height: 768

    color: "#464646"
        // Product list viewer
        Rectangle {
            id:listContainer
            anchors.left: window.left
            anchors.right: gridContainer.left
            anchors.top: window.top
            anchors.bottom: bottomBar.top

            color: "#282828"

            ListView {
                id: list
                highlightRangeMode: ListView.NoHighlightRange
                anchors.fill: parent
                model: listModel
                delegate: ListDelegate {}
                highlight: highlight
                highlightFollowsCurrentItem: true
                focus: true
                currentIndex: (count - 1)
            }

        }

        Button {
            id:holdButton
            anchors.top: gridContainer.bottom
            anchors.left: listContainer.right
            text: "Hold"
            buttonColor: "#965D60"
            onButtonClick: {list.currentIndex = 0; modelManager.holdSale(); }
            }

        Button {
            id:restoreButton
            anchors.top: gridContainer.bottom
            anchors.right: window.right
            text: "Restore"
            buttonColor: "#97B88D"
            onButtonClick: {
                if (holdModel.rowCount() > 0)
                {
                    modelManager.restoreSale(hlist.currentIndex);
                    list.currentIndex = (list.count - 1);
                    informationBar.state = "stage1loading";}
                }
            }

        Button {
            id:receiptButton
            anchors.right: window.right
            anchors.bottom: informationBar.top
            anchors.bottomMargin: 36
            text: "Receipt"
            buttonColor: "#025e73"
            onButtonClick: {
                if(informationBar.state == "stage3") {
                    modelManager.printReciept()
                }
            }
        }


        Rectangle {
            id:holdListContainer
            anchors.top: gridContainer.bottom
            anchors.left: holdButton.right
            anchors.right: restoreButton.left
            anchors.bottom: informationBar.top
            anchors.rightMargin: 5
            anchors.leftMargin: 5
            color: "#282828"

            ListView {
                id: hlist
                anchors.fill: parent
                model: holdModel
                delegate: HoldListDelegate {}

            }

        }

        //Bottom Bar containing the total sum and clear button
        Rectangle {
            id:sumBar
            anchors.bottom: bottomBar.top
            anchors.left: window.left
            anchors.right: gridContainer.left
            height:30
            color: "#282828"

            Text {
                text: "Total: " + listModel.sum + " €"
                color: "#f1f1f1"
                anchors.right: parent.right
                anchors.rightMargin: 10
                verticalAlignment: Text.AlignVCenter
                font.bold: true
                font.pixelSize: 18
            }

            SmallButton {
                id:clearButton
                anchors.left: parent.left
                anchors.top: parent.top
                x: 0
                y: 20
                text: "Clear list"
            }

        }

        //Grid Containing all the buttons
        FunctionGrid {
                id:gridContainer
                color: "#464646"
                anchors.right: window.right
                anchors.top: window.top
        }
        property bool isRed : true

        // The status bar contain the InputField
        Rectangle {
            id: bottomBar
            anchors.bottom: window.bottom
            anchors.right: window.right
            anchors.left: window.left
            height: 30
            color: "#3a3a3a"
            InputField {
                id: inputBox
                width: 328
                height: 30
                Keys.onReturnPressed:  {
                    if(!cashBox.status || informationBar.state != "stage3") {
                        if(informationBar.state == "stage3"){
                            cardReader.reset();
                            modelManager.resetRemaining()
                        }
                        informationBar.state = "stage1loading"
                        modelManager.addProductById(text)
                        text = ""
                    }
                }
            }
            Text {
                id:textBox
                anchors.right: parent.right
                color: "red"
                font.pointSize: 18
                font.bold: true

                text: if (cashBox.status) {
                          text:"Please close cashbox"
                } else {
                          text:""
                      }
            }
        }
        Timer {
            interval: 3000
            running: true
            repeat: true
            onTriggered: { if(isRed) {
                    textBox.color = "white"
                    isRed = false
                } else { textBox.color = "red"; isRed = true}

            }
        }

        InformationBar {
            id: informationBar
            anchors.right: window.right
            anchors.bottom: bottomBar.top
            width: 512
        }
        RestPolling{}
}



