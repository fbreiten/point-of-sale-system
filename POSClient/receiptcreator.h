#ifndef RECEIPTCREATOR_H
#define RECEIPTCREATOR_H

#include <QObject>
#include <QtCore>
#include <QPainter>
#include "listproduct.h"

class ReceiptCreator : public QObject
{
    Q_OBJECT
public:
    void setData(int listSize, QString sum);
    void printReceipt();
    void addRecord(ListProduct *product);

private:
    void addHeader();
    void addFooter();

    QPainter *m_pPainter;
    int m_position;
    QString m_sum;
    QPixmap *m_pPixmap;
    
signals:
    
public slots:
    
};

#endif // RECEIPTCREATOR_H
