#ifndef DBHANDLER_H
#define DBHANDLER_H
#include <QtCore>
#include <QtSql>
#include <QSqlQuery>
#include <QObject>
#include "listmodel.h"
#include "listproduct.h"
#include "customer.h"
class DBHandler : public QObject
{
    Q_OBJECT
public:
    explicit DBHandler(QObject *parent = 0);
    ~DBHandler();

    void createConnection();
    bool retrieveProduct(qint64);
    bool retrieveSaleID();

    //Fetch customer id with bonus card number
    bool retrieveCustomerIDWithBonusID(QString card);
    
    bool writeSaleData(ListProduct *product);
    bool writeSaleEntity(ListModel *model);

    inline QString getName() { return m_name; }
    inline float getPrice() { return m_price; }
    inline float getDiscount() { return m_discount; }

private:
    QSqlDatabase m_db;
    //Fetched product data is stored in m_name, m_price and m_discount
    QString m_name;
    float m_price;
    float m_discount;

    qint64 m_saleID;
    qint64 m_customerID;
    bool m_isBonusCustomer;
    bool writeBonusCardData(Customer *customer);

signals:
    void customerDataUpdated();

public slots:
    bool writeCustomerData(Customer *customer);
};

#endif // DBHANDLER_H
